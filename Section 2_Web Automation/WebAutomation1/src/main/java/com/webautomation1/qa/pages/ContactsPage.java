package com.webautomation1.qa.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import com.webautomation1.qa.base.TestBase;

public class ContactsPage extends TestBase {
	@FindBy(xpath="//div[contains(@class,'ui loader')]")
	WebElement contactPageHeader;
	@FindBy(xpath="//div[contains(@class,'ui loader')]")
	WebElement checkbox;
	@FindBy(xpath="//button[contains(text(),'New')]")
	WebElement newContact;
	
	@FindBy(id="first_name")
	WebElement firstName;
	@FindBy(id="last_name")
	WebElement lastName;
	@FindBy(id="middle_name")
	WebElement middleName;
	
	@FindBy(xpath="//button[contains(text(),'Save')]")
	WebElement saveBtn;

	public ContactsPage() {
		PageFactory.initElements(driver, this);
	}
	
	public WebElement ContactPageHeader() {
		/*
		 * boolean isDisplayed = contactPageHeader.isDisplayed(); return isDisplayed;
		 */		
		return contactPageHeader;
	}
	
	public void selectContactsByName(String contactName) {
		driver.findElement(By.xpath("//td[text()='"+contactName+"']//parent::tr"
				+ "//div[contains(@class,'ui fitted read-only checkbox')]")).click();	
	}
	
	public WebElement clickOnNewContact() {
		return newContact;
	}
	
	public void createNewContact(String fName,String lName,String mName,String company,String categoryList) {
		
		firstName.sendKeys(fName);
		lastName.sendKeys(lName);
		middleName.sendKeys(mName);
		
		driver.findElement(By.cssSelector("div.ui.fluid.search.selection.dropdown")).sendKeys(company);
		driver.findElement(By.cssSelector("div.selected.item.addition")).click();

		Select select = new Select(driver.findElement(By.name("category")));
		select.selectByVisibleText(categoryList);
		saveBtn.click();
	}	
}
