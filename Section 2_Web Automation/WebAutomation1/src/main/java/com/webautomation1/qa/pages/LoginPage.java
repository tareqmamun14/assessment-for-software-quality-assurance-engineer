package com.webautomation1.qa.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.webautomation1.qa.base.TestBase;

public class LoginPage extends TestBase {
	
	@FindBy(id="sign-username")
    WebElement username;
	@FindBy(id="sign-password")
	WebElement password;
	@FindBy(xpath="//button[normalize-space()='Log in']")
	WebElement loginBtn;
	@FindBy(xpath="//a[normalize-space()='Log in']")
	WebElement loginLink;
	
	@FindBy(how = How.CSS, using ="span.user-display")
	WebElement userDisplayName;
	
	public LoginPage() {
		PageFactory.initElements(driver, this);
	}
	
	public HomePage login(String un, String pwd){
		loginLink.click();
		username.sendKeys(un);
		password.sendKeys(pwd);
		loginBtn.click();
		return new HomePage();
	}
	
	
	public WebElement UserDisplayName() {
		return userDisplayName;		
	}
	
	
	
	
}
	