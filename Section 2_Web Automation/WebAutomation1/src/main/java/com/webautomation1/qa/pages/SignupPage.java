package com.webautomation1.qa.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.webautomation1.qa.base.TestBase;

public class SignupPage extends TestBase {
	 
	//PageFactory - ObjectRepo
	
	@FindBy(id="sign-username")
    WebElement username;
	@FindBy(id="sign-password")
	WebElement password;
	@FindBy(xpath="//button[normalize-space()='Sign up']")
	WebElement signUpBtn;
	
//	@FindBy(how = How.CSS, using ="//button[normalize-space()='Sign up']")
//	WebElement signupbtn;
    
//	@FindBy(xpath="//img[contains(@class,'header item')]")
//	WebElement logo;
    
    
	//Initializing the page objects/POM
	public SignupPage() {
		PageFactory.initElements(driver, this);
	}
	
//	public boolean validateLogo() {
//		return logo.isDisplayed();
//	}
	
	public LoginPage login(String un, String pwd){
		username.sendKeys(un);
		password.sendKeys(pwd);
		signUpBtn.click();
		driver.switchTo( ).alert( ).accept();
		return new LoginPage();
	}


}
