package com.webautomation1.qa.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.webautomation1.qa.base.TestBase;

public class HomePage extends TestBase {

	@FindBy(how = How.CSS, using ="div.header.item")
	WebElement headerLogo;
	
	@FindBy(xpath="//span[contains(text(),'Contacts')]")
	WebElement contactsLink;
	
	@FindBy(xpath="//span[contains(text(),'Deals')]")
	WebElement dealsLink;
	
	@FindBy(xpath="//span[contains(text(),'Tasks')]")
	WebElement tasksLink;
	
	@FindBy(how = How.CSS, using ="span.user-display")
	WebElement userDisplayName;
	
	public HomePage() {
		PageFactory.initElements(driver, this);
	}
	
	public WebElement verifyHomePageHeaderLogo() {
		return headerLogo;		
	}
	
	public WebElement UserDisplayName() {
		return userDisplayName;		
	}
	
	public ContactsPage clickOnContactLink() {
		contactsLink.click();
		return new ContactsPage();
	}

	
	
}
