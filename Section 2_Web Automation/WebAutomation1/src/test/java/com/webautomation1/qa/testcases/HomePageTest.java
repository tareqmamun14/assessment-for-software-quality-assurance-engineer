package com.webautomation1.qa.testcases;

import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.webautomation1.qa.base.TestBase;
import com.webautomation1.qa.pages.ContactsPage;
import com.webautomation1.qa.pages.HomePage;
import com.webautomation1.qa.pages.LoginPage;
import com.webautomation1.qa.pages.SignupPage;

public class HomePageTest extends TestBase {
       SignupPage signupPage;
       HomePage homePage;
       ContactsPage contactPage;
       LoginPage loginPage;

	
	public HomePageTest() {
		super();
	}
	
	@BeforeMethod
	public void setUp(){
	    initialization();			
	    loginPage = new LoginPage();
	    //contactPage = new ContactsPage();
	    //loginPage = homePage.login(prop.getProperty("email"),prop.getProperty("password"));
	}
	
	@Test(priority=1)
	public void verifyHomePageHeaderLogoTest(){
		WebElement HeaderLogo = homePage.verifyHomePageHeaderLogo();
	    System.out.println("HomePage Logo found :"+HeaderLogo.isDisplayed());
	}
	
	@Test(priority=2)
	public void verifyUserDisplayTest(){
		WebElement DisplayName = homePage.UserDisplayName();
		Assert.assertTrue(DisplayName.isDisplayed());
	    System.out.println("Display Name :"+ DisplayName.getText());
	}
	
	@Test(priority=3)
	public void clickOnContactsPage(){
		contactPage = homePage.clickOnContactLink();
	    System.out.println("Clicked on Contacts Page");

	}


	@AfterMethod
	public void tearDown() {
		driver.quit();

     }
	}
