package com.webautomation1.qa.testcases;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.webautomation1.qa.base.TestBase;
import com.webautomation1.qa.pages.HomePage;
import com.webautomation1.qa.pages.LoginPage;
import com.webautomation1.qa.pages.SignupPage;

public class SignupPageTest extends TestBase{
    SignupPage signupPage;
	LoginPage loginPage;
	
	public SignupPageTest() {
		super(); 	
	}
	
	@BeforeMethod
	public void setUp(){
	    initialization();			
	    signupPage = new SignupPage();
	}
	
	
	@Test(priority=1)
	public void loginTest(){
		loginPage = signupPage.login(prop.getProperty("username"),prop.getProperty("password"));	 
	}
	
	@AfterMethod
	public void tearDown() {
		driver.quit();
		
		
	}
}
